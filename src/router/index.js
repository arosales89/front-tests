import { createRouter, createWebHistory } from 'vue-router'
import Gifs from '@/views/Gifs.vue'
import Favorites from '@/views/Favorites.vue'
import History from '@/views/History.vue'

const routes = [
  {
    path: '/',
    name: 'gifs',
    props: route => ({ query: route.query }),
    component: Gifs
  },
  {
    path: '/favorites',
    name: 'favorites',
    props: route => ({ query: route.query }),
    component: Favorites
  },
  {
    path: '/history',
    name: 'history',
    props: route => ({ query: route.query }),
    component: History
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
