import Moment from 'moment'

export default {
    methods: {
        datetime(value, format) {
            return Moment(value).format(format);
        }
    }
}