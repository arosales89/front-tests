import { createStore } from 'vuex'
import giphy from './modules/giphy/store'
import loader from './modules/loader/store'
import pagination from './modules/pagination/store'

export default createStore({
    modules: {
        giphy,
        loader,
        pagination
    }
});
