export default {
    gifs: state => {
        return state.apiGifs.map(apiGifsElement => {
            return {
                ...apiGifsElement,
                favorite: state.favorites.some(favoriteElement => favoriteElement.id == apiGifsElement.id)
            }
        })
    },
    totalFavorites: state => {
        return state.favorites.length;
    },
    totalHistory: state => {
        return state.history.length;
    }
}