import services from '@/services/giphyServices'
export default {
    async getGifs({commit, rootState, rootGetters}, q) {
        try{
            commit('loader/SET_LOADING', true, {root: true});
            let response = await services.getGifs({
                offset: rootGetters['pagination/offset'],
                limit: rootState.pagination.itemsPerPage,
                q: 'dog ' + q
            });
            response.data.data = response.data.data.map(e => {return {...e, favorite: false}});
            commit('SET_GIFS', response.data);
        }
        finally {
            commit('loader/SET_LOADING', false, {root: true});
        }
    }
}