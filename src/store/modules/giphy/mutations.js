export default {
    SET_GIFS(state, payload) {
        state.apiGifs = payload.data;
        state.total = payload.data.length ? payload.pagination.total_count : 0;
    },
    SET_FAVORITE(state, payload) {
        let favorites = state.favorites;
        payload.favorite = !payload.favorite;
        if(payload.favorite) {
            favorites.push(payload);
        }
        else {
            favorites = favorites.filter(e => e.id != payload.id);
        }
        localStorage.setItem('favorites', JSON.stringify(favorites));
        state.favorites = favorites;
    },
    GET_GIPHY_STORED(state) {
        state.favorites = localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')) : [];
        state.history = localStorage.getItem('history') ? JSON.parse(localStorage.getItem('history')) : [];
    },
    ADD_HISTORY(state, payload) {
        state.history.push({
            id: state.history.length + 1,
            search: payload,
            createdOn: new Date()
        });
        localStorage.setItem('history', JSON.stringify(state.history));
    }
}