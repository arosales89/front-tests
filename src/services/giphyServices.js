import api from './giphyApi'

export default {
    getGifs(params) {
        return api.get('gifs/search', {params});
    },
    getTags() {
        return api.get('tags/related', {
            params: {
                term: 'Dog'
            }
        });
    }
}