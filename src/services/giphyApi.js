import axios from 'axios'

const api = axios.create({
    baseURL: process.env.VUE_APP_GIPHY_HOST,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
    timeout: 10000
});

api.defaults.params = {
    api_key: process.env.VUE_APP_GIPHY_KEY
}

api.interceptors.request.use(async config => {
    return config;
}, function(error) {
    return Promise.reject(error)
});

export default api;